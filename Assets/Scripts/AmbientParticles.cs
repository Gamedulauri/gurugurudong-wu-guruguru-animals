﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbientParticles : MonoBehaviour {

    ParticleSystem ps;

	// Use this for initialization
	void Start () {

            //picks a random number between 0 and 1 and defines the particle system in use
            int rnd = Random.Range(0, 2);
            ps = GetComponent<ParticleSystem>();

        //Checks if currently loaded level is NOT a sealevel
        if (Application.loadedLevel <= 20 || Application.loadedLevel >= 31)
            {
            //Uses the previously determined random number to define from which direction the particles come from by rotating the emitter
                if(rnd == 0)
                {
                    var sh = ps.shape;
                    sh.rotation = new Vector3(0,90,0);
                }
                if(rnd == 1)
                {
                    var sh = ps.shape;
                    sh.rotation = new Vector3(0, -90, 0);
                }
        }
	}
	
	// Update is called once per frame
	void Update () {
        //If level is mainmenu
        if (Application.loadedLevel == 0)
        {
            //Create a new particle array from the existing particles
            ParticleSystem.Particle[] particleArr = new ParticleSystem.Particle[ps.particleCount];

            //If the linecolour picking screen is active
            if (UIManager.LineColorMenuActive == true)
            {
                //Pause the particle system and destroy all particles
                ps.Pause();
                ps.SetParticles(particleArr, 0);
            }
            if (UIManager.LineColorMenuActive == false && ps.isPaused)
            {
                //Otherwise, play the particle effect
                ps.Play();
            }
        }
    }
}
