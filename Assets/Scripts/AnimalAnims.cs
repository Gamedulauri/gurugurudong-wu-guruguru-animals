﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalAnims : MonoBehaviour {

    int tempRnd, rnd;

    // Use this for initialization
    void Start () {
        tempRnd = Random.Range(0, 6);
        //Start the animationcoroutine an randomize a number to prevent repetition
        StartCoroutine(Animations());
    }

    public IEnumerator Animations()
    {
        //Randomize a number from 0 to 5
        rnd = Random.Range(0, 6);
        //if the number randomized at the start and the number randomized above are not the same: continue onwards.
        //THIS IS TO PREVENT REPETITION IN THE ANIMATION CYCLE
        if (tempRnd != rnd)
        {
            //Check the number and play the corresponding animation
            if (rnd == 0)
            {
                GetComponent<Animator>().Play("Bunny");
            }
            else if (rnd == 1)
            {
                GetComponent<Animator>().Play("Fox");
            }
            else if (rnd == 2)
            {
                GetComponent<Animator>().Play("Chicken");
            }
            else if (rnd == 3)
            {
                GetComponent<Animator>().Play("Dog");
            }
            else if (rnd == 4)
            {
                GetComponent<Animator>().Play("Panda");
            }
            else if (rnd == 5)
            {
                GetComponent<Animator>().Play("Cat");
            }
            yield return new WaitForSeconds(10);
            //replace the tempRnd with the newest randomized number to prevent repetition on the next cycle
            tempRnd = rnd;
        }

        //prevents the animation trying to restart itself when not active
        //if active, repeat this coroutine
        if (gameObject.activeInHierarchy)
            StartCoroutine(Animations());
    }

    void OnDisable()
    {
        //Stop the animationcoroutine when the object is disabled
         StopAllCoroutines();
    }

    void OnEnable()
    {
        //Start the animationcoroutine when the object is enabled 
        StartCoroutine(Animations());
    }
}
