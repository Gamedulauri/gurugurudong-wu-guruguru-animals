﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncePad : MonoBehaviour {

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject)
        {
            //When a gameobject collides triggers the audio and the particles
            GetComponent<AudioSource>().Play();
            GetComponentInChildren<ParticleSystem>().Play();
        }
        else // when not colliding, stop the particles
            GetComponentInChildren<ParticleSystem>().Stop();
    }
}
