﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakingPlatform : MonoBehaviour {

    public float timeToBreak;
    private bool _touchedPlatform;

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            StartCoroutine("BreakPlatform");
        }

    }
    public IEnumerator BreakPlatform()
    {
        if (_touchedPlatform == false)
        {
            _touchedPlatform = true;
            for (float f = 0; f < 20; f++)
            {
                GetComponent<SpriteRenderer>().material.color = Color.Lerp(Color.white, Color.black, f / 30);
                yield return new WaitForSeconds(timeToBreak / 30);
            }
            GetComponent<BoxCollider2D>().enabled = false;
            GetComponent<SpriteRenderer>().enabled = false;
            yield return new WaitForSeconds(2);
            /*GetComponent<BoxCollider2D>().enabled = true;
            GetComponent<SpriteRenderer>().enabled = true;
            GetComponent<SpriteRenderer>().material.color = Color.white;
            _touchedPlatform = false;*/
        }

    }
}
