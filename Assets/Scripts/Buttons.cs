﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour {

    GameObject restartButton;
    GameObject pauseButton;

    public GameObject QuitPanel, ResetPanel, PausePanel, LineCreator, goalObject, player, VictoryPanel,
                      TutorialPanel1, TutorialPanel2, TutorialPanel3, NotEnoughStars, ColorPalette,
                      VictoryBackButton, VictoryRestartButton, VictoryNextButton, PopupButton, ToggleLanguageENG,
                      ToggleLanguageJPN;

    public GameObject[] Animals;
    Vector3[] animalStartPos = new Vector3[6];

    public GameObject[] Colors;

    public static int[] ColorIsActive = new int[] { 0, 1, 2, 3, 4, 5, 6, 7 };

    public Text TimerText, WinTimeText;

    public Image StarGold;
    public Image StarSilver;

    Goal goal;

    Vector3 playerPos, playerVel;
    Quaternion playerRot;
    Rigidbody2D playerRb;

    Transform[] allMovables;
    Vector3[] movablePos;
    Vector3[] movableVel;
    Quaternion[] movableRot;

    GameObject[] tempAllDestructables;
    Transform[] allDestructables;
    Vector3[] destructablePos;
    Quaternion[] destructableRot;

    public GameObject destroyable;

    float textTimer;
    float starTimer;

    bool pauseActive;

    float levelNumberTimer;


    // Use this for initialization
    void Start()
    {
        //Set HitGoal bool from Goal script to false.
        Goal.HitGoal = false;

        //Checks if the current scene is same or higher than 1 and is not Credits.
        if(Application.loadedLevel >= 1 && Application.loadedLevelName != "Credits")
        {
            PlayAnimationsAtStart();
            FindObjectsAtStart();
        }

        CheckForMovables();
        CheckForDestroyables();
        SaveAnimalsPos();

    }

    void Update()
    {

        //If current scene is 0, checks what color is currently active.
        if(Application.loadedLevel == 0)
        {
            CheckActiveColor();
            CheckLanguage();           
        }

        /// Call these methods.
        PhoneBackButton();
        Timers();
        TimerLanguage();
        StarTimers();
        DisableIngameButtons();
        ///

        //Check if current scene is same or higher than 1.
        if(Application.loadedLevel >= 1)
        {
            EnableVictoryPanelButtons();
        }
    }

    //Method for opening level selection, plays an audio and sets level selection active, deactivates main menu and line color change menu.
    public void ToLevelSelectionButton()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        UIManager.LevelSelectionActive = true;
        UIManager.MainMenuActive = false;
        UIManager.LineColorMenuActive = false;    
    }

    //Method for opening line color change menu, plays an audio and sets line color change menu active, deactivates main menu and level selection, sets LineCreator active.
    public void ToLineColorButton()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        global::LineCreator.touching = false;
        UIManager.LevelSelectionActive = false;
        UIManager.MainMenuActive = false;
        UIManager.LineColorMenuActive = true;
        LineCreator.SetActive(true);
    }

    //Method for going back to main menu in menus. Plays an audio, sets main menu active, deactivates level selection and line color change menu,
    //Starts coroutine for the animal animations in the main menu, sets LineCreator inactive and finds objects with tag "Line" and destroys them.
    //Sets animals 3 and 4 inactive and places all animals to their temporarily saved position.
    public void BackButton()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        UIManager.MainMenuActive = true;
        UIManager.LevelSelectionActive = false;
        UIManager.LineColorMenuActive = false;
        LineCreator.SetActive(false);
        GameObject[] allLines = GameObject.FindGameObjectsWithTag("Line");
        for (var i = 0; i < allLines.Length; i++)
        {
            Destroy(allLines[i]);
        }
        Animals[2].SetActive(false);
        Animals[3].SetActive(false);

        Animals[0].transform.position = animalStartPos[0];
        Animals[1].transform.position = animalStartPos[1];
        Animals[2].transform.position = animalStartPos[2];
        Animals[3].transform.position = animalStartPos[3];
        Animals[4].transform.position = animalStartPos[4];
        Animals[5].transform.position = animalStartPos[5];

    }

    //Method for resetting all drawings in the line color change menu by destroying them.
    public void ResetDraw()
    {
        GameObject[] allLines = GameObject.FindGameObjectsWithTag("Line");
        for (var i = 0; i < allLines.Length; i++)
        {
            Destroy(allLines[i]);
        }
    }

    //Method for going back to level selection while in a level. Plays an audio and sets level selection active and timescale to 1.
    public void LevelBackButton()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        SceneManager.LoadScene(0);
        UIManager.LevelSelectionActive = true;
        UIManager.MainMenuActive = false;
        UIManager.LineColorMenuActive = false;
        Time.timeScale = 1;
    }

    //Method for going to next level after completing a stage, plays an audio when called.
    public void NextLevelButton()
    {
        Debug.Log(PlayerPrefs.GetInt("TotalScore") >= (PlayerPrefs.GetInt("LevelSet") + 1) * 6);
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        //Checks if TotalScore is same or higher than current loaded level and its remainder with 5 is 0, then loads the next level.
        if (Application.loadedLevel % 5 == 0 && PlayerPrefs.GetInt("LevelSet") * 5 >= Application.loadedLevel)
        {
            SceneManager.LoadScene(Application.loadedLevel + 1);
        }
        //Same as above but else if TotalScore is less than current loaded level and its remainder with 5 is zero, 
        //then starts coroutine to inform the player doesn't have enough stars.
        else if(PlayerPrefs.GetInt("TotalScore") < (PlayerPrefs.GetInt("LevelSet") + 1) * 6 && Application.loadedLevel % 5 == 0 && 
                (PlayerPrefs.GetInt("LevelSet") + 1) * 5 >= Application.loadedLevel)
        {
            Debug.Log("hello");
            StartCoroutine(NotEnough());
        }
        //If current loaded level's remainder with 5 is not zero, then load next level.
        if (Application.loadedLevel % 5 != 0)
        {
            SceneManager.LoadScene(Application.loadedLevel + 1);
        }
    }

    //Method for pausing the game, plays an audio, sets timescale to 0 and plays an animation, sets pauseActive to true.
    public void PauseButton()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        Time.timeScale = 0f;
        PausePanel.GetComponent<Animator>().Play("Pause");
        pauseActive = true;

    }

    //Method for disabling the pause menu, plays an audio, plays an animation, sets pauseActive to false and sets timescale to 1.
    public void ContinueButton()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        PausePanel.GetComponent<Animator>().Play("Unpause");
        pauseActive = false;
        Time.timeScale = 1;
    }

    //Method for opening Google forms for feedback
    public void FeedBackButton()
    {
        Application.OpenURL("https://goo.gl/forms/wypBiJVN6Y1RlXHi1");
    }

    //Method for setting current color to red, plays an audio and sets LineColor PlayerPref to 3.
    public void RedColor()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        PlayerPrefs.SetInt("LineColor", 3);      
    }

    //Method for setting current color to yellow, plays an audio and sets LineColor PlayerPref to 1.
    public void YellowColor()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        PlayerPrefs.SetInt("LineColor", 1);
    }

    //Method for setting current color to purple, plays an audio and sets LineColor PlayerPref to 2.
    public void PurpleColor()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        PlayerPrefs.SetInt("LineColor", 2);
    }

    //Method for setting current color to black, plays an audio and sets LineColor PlayerPref to 0.
    public void BlackColor()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        PlayerPrefs.SetInt("LineColor", 0);
    }

    //Method for setting current color to blue, plays an audio and sets LineColor PlayerPref to 4.
    public void BlueColor()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        PlayerPrefs.SetInt("LineColor", 4);
    }

    //Method for setting current color to green, plays an audio and sets LineColor PlayerPref to 5.
    public void GreenColor()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        PlayerPrefs.SetInt("LineColor", 5);
    }

    //Method for setting current color to pink, plays an audio and sets LineColor PlayerPref to 6
    public void PinkColor()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        PlayerPrefs.SetInt("LineColor", 6);
    }

    //Method for setting current color to orange, plays an audio and sets LineColor PlayerPref to 7.
    public void OrangeColor()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        PlayerPrefs.SetInt("LineColor", 7);
    }

    //Method for opening Credits scene.
    public void CreditsButton()
    {
        SceneManager.LoadScene("Credits");
    }

    //Method for leaving Credits scene and opening main menu scene
    public void CreditsBackButton()
    {
        SceneManager.LoadScene(0);
    }

    //Method for hiding the popup when unlocking new levels.
    public void HideLevelUnlock()
    {
        GameObject unlockPop = GameObject.Find("LevelUnlock");
        unlockPop.GetComponent<Animator>().Play("HideLevelUnlock");
    }

    public void SetEnglish()
    {
        //Sets the opposite language button to active and sets Lanuguage PlayerPref to 1.
        GetComponentInParent<AudioSource>().Play();
        PlayerPrefs.SetInt("Language", 1);
    }

    public void SetJapanese()
    {
        //Sets the opposite language button to active and sets Lanuguage PlayerPref to 0 .
        GetComponentInParent<AudioSource>().Play();
        PlayerPrefs.SetInt("Language", 0);
    }

    //Method for skipping the level unlock animation by speeding it up massively.
    public void SkipUnlock()
    {
        GameObject.Find("LevelUnlock").GetComponentInChildren<Animator>().speed = 10000;
    }

    //Method for pressing the back button in phone.
    void PhoneBackButton()
    {
        //If current scene is a level and not Credits, and esc or back is pressed and player has not hit goal yet, then pause the game.
        if(Application.loadedLevel >= 1 && Application.loadedLevelName != "Credits" && Input.GetKeyUp(KeyCode.Escape) && Goal.HitGoal == false)
        {
            Time.timeScale = 0;
            PausePanel.GetComponent<Animator>().Play("Pause");

            pauseActive = true;
        }

        //If current scene is a level and not Credits, and esc or back is pressed while pause is active, then go back to level selection.
        if (Application.loadedLevel >= 1 && Application.loadedLevelName != "Credits"  && pauseActive == true && Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
            UIManager.LevelSelectionActive = true;
            UIManager.MainMenuActive = false;
            UIManager.LineColorMenuActive = false;
        }

        //If level selection is active and esc or back is pressed, then set main menu active, disable animals 3 and 4 and set animal's position to temporarily saved position.
        if (Application.loadedLevel == 0 && Input.GetKeyUp(KeyCode.Escape) && UIManager.LevelSelectionActive == true)
        {
            UIManager.MainMenuActive = true;
            UIManager.LevelSelectionActive = false;
            UIManager.LineColorMenuActive = false;

            Animals[2].SetActive(false);
            Animals[3].SetActive(false);
            Animals[0].transform.position = animalStartPos[0];
            Animals[1].transform.position = animalStartPos[1];
            Animals[2].transform.position = animalStartPos[2];
            Animals[3].transform.position = animalStartPos[3];
            Animals[4].transform.position = animalStartPos[4];
            Animals[5].transform.position = animalStartPos[5];
        }

        //If line color change menu is active and esc or back is pressed, then destroy all object with tag "Line", go back to main menu and set LineCreator to inactive.
        if (Application.loadedLevel == 0 && Input.GetKeyUp(KeyCode.Escape) && UIManager.LineColorMenuActive == true)
        {
            GameObject[] allLines = GameObject.FindGameObjectsWithTag("Line");
            if (allLines != null)
            {
                for (var i = 0; i < allLines.Length; i++)
                {

                    Destroy(allLines[i]);
                }
            }

            UIManager.MainMenuActive = true;
            UIManager.LevelSelectionActive = false;
            UIManager.LineColorMenuActive = false;
            LineCreator.SetActive(false);
        }
        //If current scene is Credits and esc or back is pressed, go back to main menu.
        if(Application.loadedLevelName == "Credits" && Input.GetKeyUp(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }
    }

    //Method for hiding the color palette when called.
    public void ColorDown()
    {

        ColorPalette.GetComponent<Animator>().Play("Colorpalette");
        ColorPalette.GetComponent<Animator>().SetBool("IsUp", false);
        ColorPalette.GetComponent<Animator>().SetBool("IsDown", true);
    }

    //Method for showing the color palette when called.
    public void ColorUp()
    {
        ColorPalette.GetComponent<Animator>().Play("ColorpaletteUp");
        ColorPalette.GetComponent<Animator>().SetBool("IsDown", false);
        ColorPalette.GetComponent<Animator>().SetBool("IsUp", true);
    }

    //Method for restarting the level.


    public void RestartButton()
    {
        //If player has hit the goal, play an animation for showing the buttons after restarting.
        if(Goal.HitGoal == true)
        {
            GetComponent<Animator>().Play("ShowButtons");
        } 
        //Plays an audio, sets HitGoal from Goal script to false, enables goal object's SpriteRenderer component and stops goal object's ParticleSystem.
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        VictoryPanel.GetComponentInChildren<Animator>().Play("VictoryReset");
        Goal.HitGoal = false;
        goalObject.GetComponent<SpriteRenderer>().enabled = true;
        goalObject.GetComponent<ParticleSystem>().Stop();

        if(levelNumberTimer >= 2f)
        {
            GetComponent<Animator>().Play("LevelNumberReset");
            levelNumberTimer = 0;
        }


        //Sets touching from LineCreator to false, sets player's particle effects playing to false, sets both text and star timers to 0.
        global::LineCreator.touching = false;
        PlayerParticleEffects.IsPlaying = false;
        textTimer = 0;
        starTimer = 0;

        //Both gold and silver stars' fillamount is set to 1(basically full), timescale is set to 1, timer from Goal script is set to 0.
        StarGold.fillAmount = 1;
        StarSilver.fillAmount = 1;
        Time.timeScale = 1;
        goal.Timer = 0f;

        //Enables both restart and pause buttons' Button component, plays an animation for restarting. 
        restartButton.GetComponent<Button>().enabled = true;
        pauseButton.GetComponent<Button>().enabled = true;

        //Sets player's position and rotation to temporarily saved position and rotation.
        player.transform.position = playerPos;
        player.transform.rotation = playerRot; 

        //Player's velocity and angularVelocity are set to 0.
        playerRb.velocity = playerVel;
        playerRb.angularVelocity = 0f;

        //Finds and destroys all gameobjects with tag "Line".
        GameObject[] allLines = GameObject.FindGameObjectsWithTag("Line");
        if (allLines != null)
        {
            for (var i = 0; i < allLines.Length; i++)
            {
            
                Destroy(allLines[i]);
            }
        }

        //IF RESTARTED: RESETS MOVABLES TO ORIGINAL POSITIONS
        if (allMovables != null)
        {
            for (var i = 0; i < allMovables.Length; i++)
                {
                        allMovables[i].GetComponent<Rigidbody2D>().velocity = movableVel[i];
                allMovables[i].GetComponent<Rigidbody2D>().angularVelocity = 0f;
                allMovables[i].transform.rotation = movableRot[i];
                        allMovables[i].transform.position = movablePos[i];
                }
        }
        //IF RESTARTED: RESETS DESTRUCTABLES TO ORIGINAL POSITIONS
        for (var i = 0; i < allDestructables.Length; i++)
        {
            if (tempAllDestructables[i] == null)
            {
                tempAllDestructables[i] = Instantiate(destroyable, destructablePos[i], destructableRot[i]);
            }
        }
    }

    //Coroutine for showing that player doesn't have enough stars.
    IEnumerator NotEnough()
    {
        NotEnoughStars.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        NotEnoughStars.SetActive(false);        
    }

    //Method for checking currently active color.
    void CheckActiveColor()
    {
        for (int i = 0; i < ColorIsActive.Length; i++)
        {
            if (ColorIsActive[i] == PlayerPrefs.GetInt("LineColor", i))
                Colors[i].SetActive(true);
            else
                Colors[i].SetActive(false);
            if(PlayerPrefs.HasKey("LineColor") == false)            
                PlayerPrefs.SetInt("LineColor", 0);
            
        }
    }

    //Method for enabling the correct language select button.
    void CheckLanguage()
    {
        //Checks if language is Japanese, then sets English button active.
        if (PlayerPrefs.GetInt("Language") == 0)
        {
            ToggleLanguageENG.SetActive(true);
            ToggleLanguageJPN.SetActive(false);
        }
        //Else it goes other way around.
        else
        {
            ToggleLanguageENG.SetActive(false);
            ToggleLanguageJPN.SetActive(true);
        }
    }

    //Method for timers.
    void Timers()
    {
        //Timer for the level number timer and resetting it.
        levelNumberTimer += Time.deltaTime;
        //Timer for WinTimeText
        textTimer += Time.deltaTime;
        //Timer for showing how much time is left to get stars.
        starTimer = Time.deltaTime;
    }

    //Method for checking and changing the WinTimeText language.
    void TimerLanguage()
    {
        if (Application.loadedLevelName != "Credits")
        {
            //If scene is same or higher than 1, player has not hit goal yet and the language is Japanese, update the text where the time shows. 
            if (Application.loadedLevel >= 1 && Goal.HitGoal == false && PlayerPrefs.GetInt("Language") == 0)
            {
                WinTimeText.text = "時間 - " + textTimer.ToString("F2");
            }
            //Same as above but if language is English.
            if (Application.loadedLevel >= 1 && Goal.HitGoal == false && PlayerPrefs.GetInt("Language") == 1)
            {
                WinTimeText.text = "Your time: " + textTimer.ToString("F2");
            }
        }
    }

    //Method for showing how much time is left to get stars.
    void StarTimers()
    {
        //If current scene is a level and the Timer from Goal script is lower than gold time and player has not hit the goal yet, decrease the amount of StarGold shown. 
        if (Application.loadedLevel >= 1 && Application.loadedLevelName != "Credits" && goal.Timer <= goal.goldTimer && Goal.HitGoal == false)
        {
            StarGold.fillAmount -= 1f / goal.goldTimer * starTimer;
        }

        //Same as above but decrease from StarSilver when Timer from Goal script is higher than gold time.
        if (Application.loadedLevel >= 1 && Application.loadedLevelName != "Credits" && goal.Timer > goal.goldTimer && Goal.HitGoal == false)
        {
            StarSilver.fillAmount -= 1f / (goal.silverTimer - goal.goldTimer) * starTimer;
        }
    }

    //Method for disabling pause and restart button after hitting goal, so it's not possible to click on them before the animation is over.
    void DisableIngameButtons()
    {
        //If player has hit goal and scene is same or higher than 1, disable Button components from the restart and pause buttons.
        if (Goal.HitGoal == true && Application.loadedLevel >= 1)
        {
            restartButton.GetComponent<Button>().enabled = false;
            pauseButton.GetComponent<Button>().enabled = false;
        }
    }

    //Method for enabling/disabling buttons from the victorypanel if the animation is running or not.
    void EnableVictoryPanelButtons()
    {
        //If player has hit the goal and victory animation is playing, disable the button components from the buttons in the victory panel.
        if (Goal.HitGoal == true && Goal.VictoryAnimPlaying == true)
        {
            VictoryBackButton.GetComponent<Button>().enabled = false;
            VictoryNextButton.GetComponent<Button>().enabled = false;
            VictoryRestartButton.GetComponent<Button>().enabled = false;
        }
        //Same as above but if the victory animation is not playing, enable the button components.
        if (Goal.HitGoal == true && Goal.VictoryAnimPlaying == false)
        {
            VictoryBackButton.GetComponent<Button>().enabled = true;
            VictoryNextButton.GetComponent<Button>().enabled = true;
            VictoryRestartButton.GetComponent<Button>().enabled = true;
        }
    }

    //Method for saving animals position at main menu.
    void SaveAnimalsPos()
    {
        //If current scene is 0, temporarily save animal's position and finds AnimalAnims gameobject.
        if (Application.loadedLevel == 0)
        {
            animalStartPos[0] = Animals[0].transform.position;
            animalStartPos[1] = Animals[1].transform.position;
            animalStartPos[2] = Animals[2].transform.position;
            animalStartPos[3] = Animals[3].transform.position;
            animalStartPos[4] = Animals[4].transform.position;
            animalStartPos[5] = Animals[5].transform.position;
        }
    }

    //Method for checking for movable objects.
    void CheckForMovables()
    {
        //CHECKS FOR MOVABLES AND RECORDS THEIR POSITION AND ROTATION
        GameObject[] tempAllMovables = GameObject.FindGameObjectsWithTag("Movable");

        movablePos = new Vector3[tempAllMovables.Length];
        movableVel = new Vector3[tempAllMovables.Length];
        movableRot = new Quaternion[tempAllMovables.Length];

        allMovables = new Transform[tempAllMovables.Length];

        if (allMovables != null)
        {
            for (var i = 0; i < tempAllMovables.Length; i++)
            {
                allMovables[i] = tempAllMovables[i].GetComponent<Transform>();

                movablePos[i] = allMovables[i].transform.position;
                movableRot[i] = allMovables[i].transform.rotation;
                movableVel[i] = allMovables[i].GetComponent<Rigidbody2D>().velocity;
            }
        }
    }

    //Method for checking for destroyable objects.
    void CheckForDestroyables()
    {
        //CHECKS FOR DESTRUCTABLES AND RECORDS THEIR POSITION AND ROTATION
        tempAllDestructables = GameObject.FindGameObjectsWithTag("Destructable");

        destructablePos = new Vector3[tempAllDestructables.Length];
        destructableRot = new Quaternion[tempAllDestructables.Length];

        allDestructables = new Transform[tempAllDestructables.Length];

        if (allDestructables != null)
            for (var i = 0; i < tempAllDestructables.Length; i++)
            {
                allDestructables[i] = tempAllDestructables[i].GetComponent<Transform>();

                destructablePos[i] = allDestructables[i].transform.position;
                destructableRot[i] = allDestructables[i].transform.rotation;
            }
    }

    //Method for playing these animations at start.
    void PlayAnimationsAtStart()
    {
        GetComponent<Animator>().Play("LevelNumberReset");
        GetComponent<Animator>().Play("ShowButtons");
    }

    //Method for finding objects and saving player's position and rotation.
    void FindObjectsAtStart()
    {
        //Finds objects.
        goalObject = GameObject.FindWithTag("Goal");
        goal = goalObject.GetComponent<Goal>();
        restartButton = GameObject.Find("RestartButton");
        pauseButton = GameObject.Find("PauseButton");

        //Finds and temporarily saves player object's position, rotation, Rigidbody2D and its velocity.
        player = GameObject.FindWithTag("Player");
        playerPos = player.transform.position;
        playerRot = player.transform.rotation;
        playerRb = player.GetComponent<Rigidbody2D>();
        playerVel = playerRb.velocity;
    }
}

