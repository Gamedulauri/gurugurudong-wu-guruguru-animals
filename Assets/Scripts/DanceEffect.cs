﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DanceEffect : MonoBehaviour {

    public bool rightFirst, leftFirst, growFirst, shrinkFirst;

    public float rotation = 45;
    public float rotationSpeed = 5f;

    public float pulsateSpeed = 1f;
    public float maxSize = 2f;
    public float minSize = 1.5f;
    public float size = 1.5f;

    // Use this for initialization
    void Start () {
        //Checks what bool the gameobject has selected and starts moving it accordingly
        if(rightFirst)
            StartCoroutine(RotateRight());

        if(leftFirst)
            StartCoroutine(RotateLeft());

        if(growFirst)
            StartCoroutine(Grow());

        if (shrinkFirst)
            StartCoroutine(Shrink());
	}

    IEnumerator RotateRight()
    {
        //creates a new "rotation timer" and sets it to 0
        float rot = 0f;
        //while the "rotation timer" hasn't reached the determined speed value
        while (rot < rotationSpeed)
        {
            //The object rotates around itself
            transform.RotateAround(transform.position, transform.forward, Time.deltaTime / rotationSpeed * rotation);

            //adds time to the "rotation timer" and returns to check the while loop.
            rot += Time.deltaTime;
            yield return null;
        }

        //If the while loop has been passed, move on to the RotateLeft coroutine.
        StartCoroutine(RotateLeft());
    }
    //Same as the RotateRight(), but to the other direction and the "rotation timer" value is based on previous rotations.
    IEnumerator RotateLeft()
    {

        float rot = -transform.rotation.z;
        while (rot < rotationSpeed)
        {
            transform.RotateAround(transform.position, transform.forward, -Time.deltaTime / rotationSpeed * rotation);

            rot += Time.deltaTime;
            yield return null;
        }
        StartCoroutine(RotateRight());
    }

    //Basically the same as the RotateRight() and RotateLeft(), but with size.
    IEnumerator Grow()
    {

        while (size != maxSize)
        {
            size = Mathf.MoveTowards(size, maxSize, pulsateSpeed * Time.deltaTime);

            transform.localScale = Vector3.one * size;

            yield return null;
        }

        StartCoroutine(Shrink());
    }
    //The opposite counterpart for the Grow()
    IEnumerator Shrink()
    {
        while (size != minSize)
        {
            size = Mathf.MoveTowards(size, minSize, pulsateSpeed * Time.deltaTime);

            transform.localScale = Vector3.one * size;

            yield return null;
        }

        StartCoroutine(Grow());
    }

}
