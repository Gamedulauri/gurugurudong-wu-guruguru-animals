﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Goal : MonoBehaviour
{

    public GameObject VictoryPanel;

    string currentLevelName;

    public static bool gold, silver, bronze;
    public float goldTimer, silverTimer;

    public float rotation = 45;
    public float rotationSpeed = 5f;

    public float pulsateSpeed = 1f;
    public float maxSize = 2f;
    public float minSize = 1.5f;
    float size = 1.5f;

    public float Timer;

    public static bool HitGoal;
    public static bool VictoryAnimPlaying;

    int currentTotal, tempScore;

    void Start()
    {
        //Saves how many stars the player has in total at the beginning of the level.
        currentTotal = PlayerPrefs.GetInt("TotalScore");
        tempScore = 0;

        //Sets so that the player hasn't hit the goal yet and therefor victoryscreen does not pop up
        HitGoal = false;
        VictoryAnimPlaying = false;
        gold = silver = bronze = false;

        //Starts the "dance effect" on the goal
        StartCoroutine(RotateRight());
        StartCoroutine(Grow());
    }

    void Update()
    {
        if (HitGoal == false)
        {
            //Adds Time.deltaTime to the Timer that is used in CheckScore() coroutine.
            Timer += Time.deltaTime;
        }
    }

    //A Method to check what score the player gets
    public void CheckScore()
    {
        //If The time that has passed is less than what has been set for gold timer
        if (Timer <= goldTimer)
        {
            //Player gets gold
            gold = true;
        }//Same for silver
        else if (Timer > goldTimer)
        {
            gold = false;
            silver = true;
            bronze = false;
        }//And for bronze
        if (Timer > silverTimer)
        {
            bronze = true;
            silver = false;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {       
        //Checks if the colliding object is player and the player hasn't already activated the goal.
        if (other.gameObject.tag == "Player" && HitGoal == false)
        {
            //Mark that the player has hit the goal and plays the audio and visual effects
            HitGoal = true;
            GetComponent<AudioSource>().Play();
            GetComponent<ParticleSystem>().Play();
            GetComponent<SpriteRenderer>().enabled = false;

            //Saves the name of the loaded level to a string
            currentLevelName = Application.loadedLevelName;

            //Check what score the player got from the CheckScore() method
            CheckScore();
            
            //Checks if player got gold
            if (gold == true)
            {//Checks if the player hasn't previously gotten all the stars from the level
                if (PlayerPrefs.GetInt(currentLevelName + "Score") < 3)
                {//and if player has previously had zero stars
                    if (PlayerPrefs.GetInt(currentLevelName + "Score") == 0)
                    {
                        //add 3 to the TOTAL SCORE
                        PlayerPrefs.SetInt("TotalScore", PlayerPrefs.GetInt("TotalScore") + 3);
                        tempScore = 3;
                    }//if player has previously had one star
                    else if (PlayerPrefs.GetInt(currentLevelName + "Score") == 1)
                    {
                        //add 2 to the TOTAL SCORE
                        PlayerPrefs.SetInt("TotalScore", PlayerPrefs.GetInt("TotalScore") + 2);
                        tempScore = 2;
                    }//if player has previously had two stars
                    else if (PlayerPrefs.GetInt(currentLevelName + "Score") == 2)
                    {
                        //Add 1 to the TOTAL SCORE
                        PlayerPrefs.SetInt("TotalScore", PlayerPrefs.GetInt("TotalScore") + 1);
                        tempScore = 1;
                    }

                    //Then sets this levels score to be 3
                    PlayerPrefs.SetInt(currentLevelName + "Score", 3);
                }

                Debug.Log("GOOOOOOOOOOLDDDD!!!!!");
            }//Same for silver
            else if (silver == true)
            {
                if (PlayerPrefs.GetInt(currentLevelName + "Score") < 2)
                {
                    if (PlayerPrefs.GetInt(currentLevelName + "Score") == 0)
                    {
                        PlayerPrefs.SetInt("TotalScore", PlayerPrefs.GetInt("TotalScore") + 2);
                        tempScore = 2;
                    }
                    else if (PlayerPrefs.GetInt(currentLevelName + "Score") == 1)
                    {
                        PlayerPrefs.SetInt("TotalScore", PlayerPrefs.GetInt("TotalScore") + 1);
                        tempScore = 1;
                    }

                    PlayerPrefs.SetInt(currentLevelName + "Score", 2);
                }

                Debug.Log("Silver!");
            }//And bronze
            else 
            {
                if (PlayerPrefs.GetInt(currentLevelName + "Score") < 1)
                {
                    if (PlayerPrefs.GetInt(currentLevelName + "Score") == 0)
                    {
                        PlayerPrefs.SetInt("TotalScore", PlayerPrefs.GetInt("TotalScore") + 1);
                        tempScore = 1;
                    }

                    PlayerPrefs.SetInt(currentLevelName + "Score", 1);
                }


                bronze = true;
                Debug.Log("Bronze...");
            }

            //Activate the "Stage Completed" popupwindow animation
            StartCoroutine(VictoryPopUp());
        }
    }

    
    public IEnumerator RotateRight()
    {
        //creates a new "rotation timer" and sets it to 0
        float rot = 0f;
        //while the "rotation timer" hasn't reached the determined speed value
        while (rot < rotationSpeed)
        {
            //The object rotates around itself
            transform.RotateAround(transform.position, transform.forward, Time.deltaTime / rotationSpeed * rotation);

            //adds time to the "rotation timer" and returns to check the while loop.
            rot += Time.deltaTime;
            yield return null;
        }

        //If the while loop has been passed, move on to the RotateLeft coroutine.
        StartCoroutine(RotateLeft());
    }
    //Same as the RotateRight(), but to the other direction and the "rotation timer" value is based on previous rotations.
    IEnumerator RotateLeft()
    {
        
        float rot = -transform.rotation.z;
        while (rot < rotationSpeed)
        {
            transform.RotateAround(transform.position, transform.forward, -Time.deltaTime / rotationSpeed * rotation);

            rot += Time.deltaTime;
            yield return null;
        }
        StartCoroutine(RotateRight());
    }

    //Basically the same as the RotateRight() and RotateLeft(), but with size.
    public IEnumerator Grow()
    {

        while (size != maxSize)
        {
            size = Mathf.MoveTowards(size, maxSize, pulsateSpeed * Time.deltaTime);

            transform.localScale = Vector3.one * size;

            yield return null;
        }

        StartCoroutine(Shrink());
    }
    //The opposite counterpart for the Grow()
    IEnumerator Shrink()
    {
        while (size != minSize)
        {
            size = Mathf.MoveTowards(size, minSize, pulsateSpeed * Time.deltaTime);

            transform.localScale = Vector3.one * size;

            yield return null;
        }

        StartCoroutine(Grow());
    }


    //A coroutine for the panel that pops up after getting to the goal.
    IEnumerator VictoryPopUp()
    {
        //Determines what the "New levels unlocked" popup windows parent/animatorholder is.
        GameObject UnlockPopup = GameObject.Find("LevelUnlock");
        
        //Hides the back and restart buttons, waits for a bit and activates the animation for the victoryscreen
        VictoryAnimPlaying = true;
        VictoryPanel.GetComponentInParent<Animator>().Play("HideButtons");
        yield return new WaitForSeconds(1f);
        //VictoryPanel.SetActive(true);
        VictoryPanel.GetComponentInChildren<Animator>().Play("Victory");

        yield return new WaitForSeconds(.3f);

        // Enable the "New levels unlocked"-popup if The players total amount surpasses the required amount
        if (currentTotal + tempScore >= (PlayerPrefs.GetInt("LevelSet") + 1) * 6 &&
            PlayerPrefs.GetInt("LevelSet") < 8)
        {
            //If conditions above are met, enables the "New levels unlocked" popup and marks the current level set.
            UnlockPopup.GetComponent<Animator>().Play("LevelUnlock");
            PlayerPrefs.SetInt("LevelSet", PlayerPrefs.GetInt("LevelSet") + 1);

        }
        //Saves how many stars the player has in total and resets the tempscore.
        currentTotal = PlayerPrefs.GetInt("TotalScore");
        tempScore = 0;

        //Stops the victoryanimation
        VictoryAnimPlaying = false;
    }
}
