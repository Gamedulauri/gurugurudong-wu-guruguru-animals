﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LanguageSelect : MonoBehaviour {

    public GameObject levelUnlockText;
    public GameObject notEnoughText;

    public Sprite JPN, ENG;

    void Update()
    {
        //Calls the CheckLanguage method.
        CheckLanguage();
    }

    //Method for changing the game's language.
	void CheckLanguage () {

        //If language is Japanese, set the Japanese title.
        if (PlayerPrefs.GetInt("Language") == 0 && Application.loadedLevel == 0)
        {
            GameObject.Find("Title").GetComponent<Image>().sprite = JPN;
        }
        //If language is English, set the English title.
        if(PlayerPrefs.GetInt("Language") == 1 && Application.loadedLevel == 0)
        {
            GameObject.Find("Title").GetComponent<Image>().sprite = ENG;
        }
        //If language is Japanese and current scene is Credits, set the texts in Japanese.
        if (PlayerPrefs.GetInt("Language") == 0 && Application.loadedLevelName == "Credits")
        {
            GameObject.Find("Programming").GetComponent<Text>().text = "プログラミング";
            GameObject.Find("Graphics").GetComponent<Text>().text = "グラフィックス";
            GameObject.Find("Audio").GetComponent<Text>().text = "ミュージック";
            GameObject.Find("Lauri").GetComponent<Text>().enabled = true;
            GameObject.Find("LauriENG").GetComponent<Text>().enabled = false;
            GameObject.Find("Teemu").GetComponent<Text>().enabled = true;
            GameObject.Find("TeemuENG").GetComponent<Text>().enabled = false;
            GameObject.Find("Aura").GetComponent<Text>().enabled = true;
            GameObject.Find("AuraENG").GetComponent<Text>().enabled = false;
            GameObject.Find("Emmi").GetComponent<Text>().enabled = true;
            GameObject.Find("EmmiENG").GetComponent<Text>().enabled = false;
            GameObject.Find("Lauri2").GetComponent<Text>().enabled = true;
            GameObject.Find("Lauri2ENG").GetComponent<Text>().enabled = false;
        }
        //Same as above but in English.
        if (PlayerPrefs.GetInt("Language") == 1 && Application.loadedLevelName == "Credits")
        {
            GameObject.Find("Programming").GetComponent<Text>().text = "Programming";
            GameObject.Find("Graphics").GetComponent<Text>().text = "Graphics";
            GameObject.Find("Audio").GetComponent<Text>().text = "Music";
            GameObject.Find("Lauri").GetComponent<Text>().enabled = false;
            GameObject.Find("LauriENG").GetComponent<Text>().enabled = true;
            GameObject.Find("Teemu").GetComponent<Text>().enabled = false;
            GameObject.Find("TeemuENG").GetComponent<Text>().enabled = true;
            GameObject.Find("Aura").GetComponent<Text>().enabled = false;
            GameObject.Find("AuraENG").GetComponent<Text>().enabled = true;
            GameObject.Find("Emmi").GetComponent<Text>().enabled = false;
            GameObject.Find("EmmiENG").GetComponent<Text>().enabled = true;
            GameObject.Find("Lauri2").GetComponent<Text>().enabled = false;
            GameObject.Find("Lauri2ENG").GetComponent<Text>().enabled = true;
        }
        //If language is Japanese, set the unlock text and not enough stars text to Japanese language.
        if (PlayerPrefs.GetInt("Language") == 0 && Application.loadedLevel >= 1 && Application.loadedLevelName != "Credits")
        {
            levelUnlockText.GetComponent<Text>().text = "新しいステージが解放されました!";
            notEnoughText.GetComponent<Text>().text = "十分ではない星";
        }
        //Same as above but in English.
        if (PlayerPrefs.GetInt("Language") == 1 && Application.loadedLevel >= 1 && Application.loadedLevelName != "Credits")
        {
            levelUnlockText.GetComponent<Text>().text = "You have unlocked new stages!";
            notEnoughText.GetComponent<Text>().text = "You do not have enough stars";
        }
    }

}
