﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class LevelButton : MonoBehaviour {

    public Text levelText;
    public int isUnlocked;
    public GameObject star1, star2, star3, lockObject;

    GameObject spacer;
    Vector3 pos;
    public float minY, maxY;

    Vector2 touchDeltaPosition;

    bool hasMoved;

    float timer;

    void Start()
    {
        //Defines the spacer and its starting position on which the level buttons are on
        spacer = GameObject.Find("Spacer");
        pos = spacer.transform.position;

        //Makes the buttons interactable by default
        hasMoved = false;

        //Timer for not being able to press a level button quickly after going to level selection menu.
        timer = 0;
    }
    void Update()
    {
        //Timer counts time.
        timer += Time.deltaTime;

        //If the button is pressed
        if (Input.touchCount > 0 &&
            Input.GetTouch(0).phase == TouchPhase.Began)
        {
            //Save the new position of the spacer
            pos = spacer.transform.position;
            hasMoved = false;
        }

        //if the button is pressed and the finger has moved during the pressing
        if (Input.touchCount > 0 &&
 Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            //If the spacer has moved 5 units to up or down
            if (spacer.transform.position.y <= pos.y - 5 || spacer.transform.position.y >= pos.y + 5)
            {
                //Makes the button "non interactable"
                hasMoved = true;
            }
        }

        //If the button is not being pressed anymore
        if (Input.touchCount > 0 &&
             Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            //If the finger was on top of this button and the button hasn't been moved
            GameObject eventSystem = GameObject.Find("EventSystem");
            if (eventSystem.GetComponent<EventSystem>().currentSelectedGameObject == gameObject && hasMoved == false)
            {
                //Call the LevelLoad function
                LevelLoad();
            }
        }
    }

    public void LevelLoad()
    {
        //If timer is higher .5 seconds, then its possible to load a level.
        if(timer >= .5f)
        {
            //Play the button click sound
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();

            //Save the spacers current position so that it can return to the same position when player comes back to the levelselection screen
            PlayerPrefs.SetFloat("SpacerPos",spacer.transform.localPosition.y);

            //Load the corresponding level for the button
            SceneManager.LoadScene("Level" + levelText.text);
        }

    }

    public void PopUpButton()
    {
        //Play the popup animation
        GetComponentInChildren<Animator>().Play("HidePopup");
    }


}
