﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LevelManager : MonoBehaviour {

    [System.Serializable]
    public class Level
    {
        public string levelName;
        public int unlocked;
        public bool isInteractable;
    }

    public GameObject levelButton;
    public Transform spacer;
    public List<Sprite> spriteList;
    public List<Level> levelList;

    int levelValue;

    void Start () {

        FillList();
	}

    void FillList()
    {
        foreach(var level in levelList)
        {
            //INSTANTIATES A NEW LEVEL BUTTON AND AQUIRES ITS "LevelButton" -SCRIPT
            GameObject newButton = Instantiate(levelButton) as GameObject;
            LevelButton button = newButton.GetComponent<LevelButton>();

            //DEFINES THE TEXT (NUMBER) ON THE BUTTON AND CONVERTS IT FROM STRING TO INT
            button.levelText.text = level.levelName;
            int levelNumber = int.Parse(button.levelText.text);

            //LEVEL BUTTON SPRITE SWAP IN SETS OF FIVE
            if( levelNumber <= 5)
            {
                newButton.GetComponent<Image>().sprite = spriteList[0];
            }
            if (levelNumber > 5 && levelNumber <= 10)
            {
                newButton.GetComponent<Image>().sprite = spriteList[1];
            }
            if (levelNumber > 10 && levelNumber <= 15)
            {
                newButton.GetComponent<Image>().sprite = spriteList[2];
            }
            if (levelNumber > 15 && levelNumber <= 20)
            {
                newButton.GetComponent<Image>().sprite = spriteList[3];
            }
            if (levelNumber > 20 && levelNumber <= 25)
            {
                newButton.GetComponent<Image>().sprite = spriteList[4];
            }
            if (levelNumber > 25 && levelNumber <= 30)
            {
                newButton.GetComponent<Image>().sprite = spriteList[5];
            }
            if (levelNumber > 30 && levelNumber <= 35)
            {
                newButton.GetComponent<Image>().sprite = spriteList[6];
            }
            if (levelNumber > 35 && levelNumber <= 40)
            {
                newButton.GetComponent<Image>().sprite = spriteList[7];
            }
            if (levelNumber > 40 && levelNumber <= 45)
            {
                newButton.GetComponent<Image>().sprite = spriteList[8];
            }

            //LEVEL UNLOCKING IN STACKS OF FIVE EVERYTIME THE PLAYER GETS SIX STARS
            if (levelNumber <= PlayerPrefs.GetInt("LevelSet") * 5 + 5 && 
                PlayerPrefs.GetInt("TotalScore") >= PlayerPrefs.GetInt("LevelSet") * 6)
            {
                level.unlocked = 1;
                level.isInteractable = true;
            }
          
            //IF LEVEL QUALIFIES, IT BECOMES INTERACTABLE
            button.isUnlocked = level.unlocked;
            button.GetComponent<Button>().interactable = level.isInteractable;

#if UNITY_EDITOR
            //WITH THIS THE LEVEL SELECTION MENU WORKS IN UNITY EDITOR
            //adds a listener to the button thet loads the determined level if clicked
            button.GetComponent<Button>().onClick.AddListener(() => LoadLevels("level" + button.levelText.text));
#endif

            //CHECKS THE SCORE ON THE LEVEL AND ENABLES THE STARSPRITES ACCORDINGLY
            if (PlayerPrefs.GetInt("Level" + button.levelText.text + "Score") > 0)
            {
                button.star1.SetActive(true);
            }
            if (PlayerPrefs.GetInt("Level" + button.levelText.text + "Score") > 1)
            {
                button.star2.SetActive(true);
            }
            if (PlayerPrefs.GetInt("Level" + button.levelText.text + "Score") > 2)
            {
                button.star3.SetActive(true);
            }

            //SETS THE BUTTONS PARENT TO BE THE SPACER
            newButton.transform.SetParent(spacer, false);


            // Disables the lock on top of open levels.
            if (level.isInteractable == true)
            {
                button.lockObject.SetActive(false);
            }
        }

        SaveAll();
    }


    void SaveAll()
    {
            GameObject[] allButtons = GameObject.FindGameObjectsWithTag("LevelButton");
            foreach (GameObject buttons in allButtons)
            {
                LevelButton button = buttons.GetComponent<LevelButton>();
                PlayerPrefs.SetInt("Level" + button.levelText.text, button.isUnlocked);
            }
        }


    public void LoadLevels(string value)
    {
        //LOADS THE LEVEL ACCORDING TO THE LEVELBUTTON NAME
        Application.LoadLevel(value);
    }
}
