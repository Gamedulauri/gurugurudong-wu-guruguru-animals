﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Line : MonoBehaviour {

    public LineRenderer lineRenderer, outlineLineRenderer;
    public PolygonCollider2D polyCol;
    
    public static int currentPoints;

    List<Vector2> points;

    public bool colliding, drawingActive;

    Color colour;

    private void Start()
    {
        //Sets the default for line collision to be false
        colliding = false;

        //Checks the line colour from playerprefs and sets the colour accordingly
        if (PlayerPrefs.GetInt("LineColor") <= 0 || PlayerPrefs.GetInt("LineColor") > 7)
        {
            //Black
            colour = new Color(0.294f, 0.294f, 0.294f);

            lineRenderer.SetColors(colour, colour);
        }
        if (PlayerPrefs.GetInt("LineColor") == 1)
        {
            //Yellow
            colour = new Color(0.953f, 0.894f, 0.322f);
            lineRenderer.SetColors(colour, colour);
        }
        if (PlayerPrefs.GetInt("LineColor") == 2)
        {
            //Purple
            colour = new Color(0.698f, 0.439f, 0.741f);
            lineRenderer.SetColors(colour, colour);
        }
        if (PlayerPrefs.GetInt("LineColor") == 3)
        {
            //Red
            colour = new Color(0.933f, 0.376f, 0.376f);

            lineRenderer.SetColors(colour, colour);
        }
        if (PlayerPrefs.GetInt("LineColor") == 4)
        {
            //Blue
            colour = new Color(0.224f, 0.745f, 0.871f);

            lineRenderer.SetColors(colour, colour);
        }
        if (PlayerPrefs.GetInt("LineColor") == 5)
        {
            //Green
            colour = new Color(0.22f, 0.839f, 0.486f);

            lineRenderer.SetColors(colour, colour);
        }
        if (PlayerPrefs.GetInt("LineColor") == 6)
        {
            //Pink
            colour = new Color(0.973f, 0.494f, 0.761f);

            lineRenderer.SetColors(colour, colour);
        }
        if (PlayerPrefs.GetInt("LineColor") == 7)
        {
            //Orange
            colour = new Color(0.984f, 0.675f, 0.314f);

            lineRenderer.SetColors(colour, colour);
        }
    }

    void Update()
    {

        //Check how many points are in the points list for the LineCreator script
        currentPoints = points.Count;

        //If colliding change transparency.
        if (colliding == true)
        {
            lineRenderer.SetColors(new Color(colour.r, colour.g, colour.b, 0.4f), new Color(colour.r, colour.g, colour.b, 0.4f));
            outlineLineRenderer.SetColors(new Color(colour.r, colour.g, colour.b, 0.4f), new Color(colour.r, colour.g, colour.b, 0.4f));
        }
        else if(colliding == false)
        {
            //Return to original colours
            lineRenderer.SetColors(colour, colour);
            outlineLineRenderer.SetColors(Color.black,Color.black);
        }

        //If y position low enough, destroy object.
        if (transform.position.y <= -300)
            Destroy(gameObject);

        //Call for collisioncheck
        CollisionRaycast();

    }

    //Function for updating the line. variable mousePos is determined in LineCreator Script
    public void UpdateLine(Vector2 mousePos)
    {
        //If no points exist
        if (points == null)
        {
            //Create a new line by creating a new points list 
            points = new List<Vector2>();

            //Calls the SetPoint function with mousePos replacing the SetPoints point variable
            SetPoint(mousePos);
            return;
        }

        //If distance between the last point and the current position of the mouse is bigger than 1
        if (Vector2.Distance(points.Last(), mousePos) > 1f)
        {
            //Calls the SetPoint function with mousePos replacing the SetPoints point variable
            SetPoint(mousePos);
        }

    }

    void SetPoint(Vector2 point)
    {
            //Defines the half width of the line. Takes the outline in consideration
            float halfWidth = (lineRenderer.startWidth / 2f) + ((outlineLineRenderer.startWidth - lineRenderer.startWidth) / 2f);

        //Adds a point to the points list to the current mouseposition. (CHECK UpdateLine() FOR MORE INFO)
            points.Add(point);

        //Sets the line renderers pointposition count to be the same as the lines and defines its position accordingly
            lineRenderer.positionCount = points.Count;
            lineRenderer.SetPosition(points.Count - 1, point);

        //The same but for the outline
        outlineLineRenderer.positionCount = points.Count;
        outlineLineRenderer.SetPosition(points.Count - 1, point);

        //ADDING THE COLLIDERS
        //Creates a new colPoints list for the polygonCollider2D points positions.
        List<Vector2> colPoints = new List<Vector2>();

        for (int i = 0; i < points.Count; i++)
        {
            //If the point is not the first on the list
            if (points[i] != points[0])
            {
                //Check the direction from the previous point to the current point
                Vector2 heading = points[i - 1] - points[i];

                //Gets the rotation from the heading variable and "rotates" it.
                //Check https://docs.unity3d.com/ScriptReference/Vector3.Cross.html for a more indepth explanation
                Vector3 crossProduct = Vector3.Cross(heading, Vector3.forward);

                //Determines colliderpoints to halfway distance from crossproduct.
                //Other point goes halfway - points position and the other halfway + points position.
                //This way we can have an accurate edge on the line renderer
                Vector2 up = halfWidth * new Vector2(crossProduct.normalized.x, crossProduct.normalized.y) + points[i - 1];
                Vector2 down = -halfWidth * new Vector2(crossProduct.normalized.x, crossProduct.normalized.y) + points[i - 1];

                //Add the points to the colPoints list
                colPoints.Insert(0, down);
                colPoints.Add(up);

                //This does the almost the same as above. This is to prevent "colliderless" parts at the end of the lines
                if (i == points.Count - 1)
                {
                    up = halfWidth * new Vector2(crossProduct.normalized.x, crossProduct.normalized.y) + points[i];
                    down = -halfWidth * new Vector2(crossProduct.normalized.x, crossProduct.normalized.y) + points[i];

                    colPoints.Insert(0, down);
                    colPoints.Add(up);
                }

            }
        }
        //If there are more points than one, add colPoints info to the PolygonCollider2D
        if (points.Count > 1)
            polyCol.points = colPoints.ToArray();
    }


    //Checks collision with raycast.
    void CollisionRaycast()
    {
        //If player is currently drawing. This is determined in the LineCreator script
        if (drawingActive)
        {
            // For each point
            for (int i = 0; i < points.Count; i++)
            {
                //If the point is not the first one
                if (points[i] != points[0])
                {
                    //Checks the direction from the previous point to this one and gets the distance between them
                    var heading = points[i - 1] - points[i];
                    var distance = heading.magnitude;

                    //Make a raycast to go from this point to the previous one
                    RaycastHit2D hit = Physics2D.Raycast(points[i], heading, distance);
                    if (hit)
                    {
                        //If something obstructed the raycast
                        if (hit.collider != null)
                        {
                            //Mark that the line is currently colliding with something
                            colliding = true;
                            //Then return to check again
                            return;
                        }
                    }// If the raycast is not obstructed (anymore)
                    else 
                    {//and if colliding is true, set colliding to be false
                        if (colliding == true)
                            colliding = false;
                    }

#if UNITY_EDITOR
                    //THIS IS TO VISUALIZE THE RAY BETWEEN THE POINTS IN UNITY EDITOR

                    // Debug.DrawRay(points[i], heading, Color.red, Mathf.Infinity);
#endif
                }

            }
        }
    }
}