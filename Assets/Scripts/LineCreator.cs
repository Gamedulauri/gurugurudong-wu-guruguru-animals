﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LineCreator : MonoBehaviour
{

    public GameObject linePrefab;

    Line activeLine;
    Rigidbody2D activeLineRb;
    PolygonCollider2D lineColliders;
    public static bool touching;

    void Awake()
    {
        //Determines that the player is not touching and there are no lines created yet.
        touching = false;
        Line.currentPoints = 0;
    }

    void Update()
    {
        UnityInput();
    }

    void UnityInput()
    {
        //If finger is over UI
        if (EventSystem.current.IsPointerOverGameObject(0))
        {
            // ui touched
        }//If game is not paused and player hasn't hit the goal yet
        else if (Time.timeScale == 1f && Goal.HitGoal == false)
        {
            //If mouse is pressed or finger touches the screen
            if (Input.GetMouseButtonDown(0))
            {
                //creates a new line and aquires its necessary components
                GameObject lineGO = Instantiate(linePrefab);
                activeLine = lineGO.GetComponent<Line>();
                activeLineRb = lineGO.GetComponent<Rigidbody2D>();
                lineColliders = lineGO.GetComponent<PolygonCollider2D>();

                //informs the Line script that the player is currently drawing
                activeLine.drawingActive = true;
            }

            //If for some reason (restart etc.) the active line isn't there anymore, return.
            if(activeLine == null)
            {
                return;
            }

            //If finger is raised, the line isn't colliding, the line has more than 1 point and loaded scene isn't the main menu scene
            //OR if the player has reached the goal, but is still drawing
            if (Input.GetMouseButtonUp(0) && activeLine.colliding == false && Line.currentPoints > 1 && Application.loadedLevel != 0)
            {
                //IF PERFORMANCE ISSUES ARISE, SOHULD TRY TO SIMPLIFY THE LINES LIKE DONE BELOW.
                //Perhaps continous simplification though. And in a different function of course.
                //activeLine.GetComponent<LineRenderer>().Simplify(1);

                //Inform the Line script that the player isn't drawing anymore and make the active line null again.
                activeLine.drawingActive = false;
                activeLine = null;

                //Change the rigidbodytype to be dynamic so that the line falls and enable it's colliders
                activeLineRb.bodyType = RigidbodyType2D.Dynamic;
                lineColliders.enabled = true;

                //Calculate the gravity for the line by using its mass
                activeLineRb.gravityScale = activeLineRb.gravityScale + (activeLineRb.mass / 7);
                if (activeLineRb.gravityScale > 20)
                    activeLineRb.gravityScale = 20;
            }

            //If the loaded scene is the colourpicker scene and the finger is raised
            if (Application.loadedLevel == 0 && Input.GetMouseButtonUp(0) && Line.currentPoints > 1)
            {
                //Inform the Line script that the player isn't drawing anymore and make the active line null again.
                activeLine.drawingActive = false;
                activeLine = null;
            }
            
            //If for some reason (restart etc.) the active line isn't there anymore, return.
            if (activeLine == null)
            {
                return;
            }//Otherwise if the line is colliding or it has only one point or two fingers touch the screen
            else if (Input.GetMouseButtonUp(0) && activeLine.colliding == true || Input.GetMouseButtonUp(0) && Line.currentPoints < 2 || Input.touchCount > 1)
            {
                //Inform the Line script that the player isn't drawing anymore and destroy the created line
                activeLine.drawingActive = false;
                Destroy(activeLine.gameObject);
            }

            //If there is a line being drawn and only one finger is on the screen
            if (activeLine != null && Input.touchCount == 1)
            {
                //Take the current x and y positions of the mouse/finger
                Vector3 tempPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10f);
                Vector3 mousePos = Camera.main.ScreenToWorldPoint(tempPos);

                //Inform the Line scripts UpdateLine function with the current mouse position
                activeLine.UpdateLine(mousePos);

                //Calculate the mass of the lines by using the its points count
                    activeLineRb.mass = Line.currentPoints / 4;
            }
           
#if UNITY_EDITOR
            //THE SAME AS ABOVE, BUT BECAUSE TOUCHCOUNTS DONT WORK WITH UNITY EDITOR THIS HAS TO BE SEPARATELY
            if (activeLine != null)
            {
                Vector3 tempPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10f);
                Vector3 mousePos = Camera.main.ScreenToWorldPoint(tempPos);
                activeLine.UpdateLine(mousePos);

                activeLineRb.mass = Line.currentPoints / 4;
            }
#endif
        }

        //If for some reason (restart etc.) the active line isn't there anymore, return.
        if (activeLine == null)
        {
            return;
        }//If the player hits the goal but is still drawing
        else if (Line.currentPoints > 1 && activeLine.colliding == false && Goal.HitGoal == true)
        {
            //Inform the Line script that the player isn't drawing anymore and make the active line null again.
            activeLine.drawingActive = false;
            activeLine = null;

            //Change the rigidbodytype to be dynamic so that the line falls and enable it's colliders
            activeLineRb.bodyType = RigidbodyType2D.Dynamic;
            lineColliders.enabled = true;

            //Calculate the gravity for the line by using its mass
            activeLineRb.gravityScale = activeLineRb.gravityScale + (activeLineRb.mass / 7);
            if (activeLineRb.gravityScale > 20)
                activeLineRb.gravityScale = 20;
        }//If the player hits the goal but is still drawing AND the drawing is colliding with something
        else if(activeLine.colliding == true && Goal.HitGoal == true)
        {
            //Inform the Line script that the player isn't drawing anymore and destroy the created line
            activeLine.drawingActive = false;
            Destroy(activeLine.gameObject);
        }
    }
}

