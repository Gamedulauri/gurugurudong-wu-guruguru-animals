﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LockScript : MonoBehaviour {

    public float rotation = 45;
    public float rotationSpeed = 5f;

    int shakeTimes;


    // Use this for initialization
    void Start () {
        //Start the shaking
        StartCoroutine(RotateRight());
	}

    IEnumerator RotateRight()
    {
        //Add 1 to the times the lock has shook
        shakeTimes++;

        //creates a new "rotation timer" and sets it to 0
        float rot = 0f;
        //while the "rotation timer" hasn't reached the determined speed value
        while (rot < rotationSpeed)
        {
            //The object rotates around itself
            transform.RotateAround(transform.position, transform.forward, Time.deltaTime / rotationSpeed * rotation);

            //adds time to the "rotation timer" and returns to check the while loop.
            rot += Time.deltaTime;
            yield return null;
        }

        //If the lock has shook more than 5 times
        if (shakeTimes > 5)
        {
            //Start the idle coroutine
            StartCoroutine(WaitForBit());
        }
        else
        {//otherwise move on to rotating to the other direction
            StartCoroutine(RotateLeft());
        }
    }

    //Same as the RotateRight(), but to the other direction and the "rotation timer" value is based on previous rotations.
    IEnumerator RotateLeft()
    {
        float rot = -transform.rotation.z;
        while (rot < rotationSpeed)
        {
            transform.RotateAround(transform.position, transform.forward, -Time.deltaTime / rotationSpeed * rotation);

            rot += Time.deltaTime;
            yield return null;
        }

            StartCoroutine(RotateRight());
    }

    IEnumerator WaitForBit()
    {
        //Stops all coroutines and resets the time the lock has shook.
        StopCoroutine(RotateRight());
        StopCoroutine(RotateLeft());
        shakeTimes = 0;

        //waits for a few seconds and restarts the cycle
        yield return new WaitForSeconds(2f);
        StartCoroutine(RotateLeft());
    }

    public void Popup()
    {
        //Popup animation for "how many stars are needed to unlock" -panel
        GetComponentInParent<Animator>().Play("StarAmountPopup");        
    }
}
