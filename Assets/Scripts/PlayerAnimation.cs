﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour {

    public Sprite Sprite1;
    public Sprite Sprite2;

    SpriteRenderer sr;

    Rigidbody2D rb;

    float currentSpeed;

    bool audioHasPlayed;

    private void Start()
    {
        //Define the RpriteRenderer and Rigidbody2D
        sr = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();

        //Audio for bouncing off the walls default is set to false
        audioHasPlayed = false;
    }
    void Update()
    {
        //Checks the players current velocity
        currentSpeed = rb.velocity.magnitude;

        //If the velocity is high enough
        if(currentSpeed > 50)
        {
            //Switch the sprite
            sr.sprite = Sprite2;
        }
        else
        {
            //switch to original sprite
            sr.sprite = Sprite1;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //If the player collides with something other than the goal or a bouncypad
            if (collision.gameObject.tag != "Goal" && collision.gameObject.tag != "Bouncy")
            {
            //And if the players velocity is high enough and audio hasn't played yet
                if (rb.velocity.magnitude > 100 && audioHasPlayed == false)
                {
                //find the audiosource and play the audio
                    GameObject aSource = GameObject.Find("PlayerParticles");
                    aSource.GetComponent<AudioSource>().Play();
                    audioHasPlayed = true;

                    Debug.Log("Boing!");
                }
            }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag != "Goal")
            audioHasPlayed = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //when exiting the ´collision set the audio to be playable again
        if(collision.gameObject.tag != "Goal")
            audioHasPlayed = false;
    }

    private void OnMouseDown()
    {
        //If player is clicked, play an audioclip
        GetComponent<AudioSource>().Play();
    }
}
