﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerParticleEffects : MonoBehaviour {

    GameObject HeartParticles;
    GameObject DustParticles;
    ParticleSystem particles;
    Rigidbody2D rb;
    
    public static bool IsPlaying;

    bool dustPlaying;

    private void Start()
    {
        //Find the required gameobjects and components and set the emitter to NOT be playing by default
        DustParticles = GameObject.Find("DustParticles");
        HeartParticles = GameObject.Find("PlayerParticles");
        rb = GetComponent<Rigidbody2D>();
        particles = HeartParticles.GetComponentInChildren<ParticleSystem>();
        IsPlaying = false;
    }
    void Update()
    {
        //if the emitter is not playing
        if (IsPlaying == false)
        {
            //stop the particle effect and destroy all the particles
            particles.Stop();
            ParticleSystem.Particle[] particleArr = new ParticleSystem.Particle[particles.particleCount];
            particles.SetParticles(particleArr, 0);
        }

        //If player goes fast enough and dust particles aren't already enabled
        if(rb.velocity.magnitude > 55 && dustPlaying == false)
        {
            //enable dust particles
            dustPlaying = true;
            DustParticles.GetComponent<ParticleSystem>().Play();
        }//otherwise
        else if(rb.velocity.magnitude < 55 && dustPlaying == true)
        { 
            //Disable dust particles
            dustPlaying = false;
            DustParticles.GetComponent<ParticleSystem>().Stop();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //If the player hits the goal
        if (collision.gameObject.tag == "Goal" && IsPlaying == false)
        {
            //Activate particle effect
            particles.Play();
            IsPlaying = true;
        }
    }
}
