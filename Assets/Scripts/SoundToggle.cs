﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundToggle : MonoBehaviour {

    public GameObject SoundOnButton;
    public GameObject SoundOffButton;

	// Update is called once per frame
	void Update ()
    {
        //If audio is set to be muted
		if(PlayerPrefs.GetInt("AudioMute") == 1)
        {
            //Mute audio
            SoundOnButton.SetActive(false);
            SoundOffButton.SetActive(true);
            AudioListener.volume = 0;

        }

        //If audio is NOT muted
        if (PlayerPrefs.GetInt("AudioMute") == 0)
        {
            //Enable audio
            SoundOnButton.SetActive(true);
            SoundOffButton.SetActive(false);
            AudioListener.volume = 1;

        }
	}

    //Enables audio and switches between sprites. Triggered through the mute/unmute button
    public void AudioOn()
    {
        SoundOnButton.SetActive(false);
        SoundOffButton.SetActive(true);
        PlayerPrefs.SetInt("AudioMute", 1);
    }

    //Disables audio and switches between sprites. Triggered through the mute/unmute button
    public void AudioOff()
    {
        SoundOnButton.SetActive(true);
        SoundOffButton.SetActive(false);
        PlayerPrefs.SetInt("AudioMute", 0);

        //Plays a button click sound
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
    }
}
