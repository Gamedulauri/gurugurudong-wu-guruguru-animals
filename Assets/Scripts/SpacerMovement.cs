﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpacerMovement : MonoBehaviour {

    static bool firstStart = true;
	// Use this for initialization

	void Start () {
        //If this is the first time main menu is accessed in this playtime
        if(firstStart == true)
        {
            //Reset the spacer/levelbuttons position and mark that this was the first time
            PlayerPrefs.DeleteKey("SpacerPos");
            firstStart = false;
        }

        // If the spacers position has been reseted
	if (PlayerPrefs.HasKey("SpacerPos") == false)
        {
            //move the spacer to its default position
            transform.localPosition = new Vector2(transform.localPosition.x, -500);
            Debug.Log("No SpacerPos value found. Spacer is at default position.");
        }
        else
        {
            //Move the spacer to where it was last left to
            transform.localPosition = new Vector2(transform.localPosition.x, PlayerPrefs.GetFloat("SpacerPos"));
        }

	}
}
