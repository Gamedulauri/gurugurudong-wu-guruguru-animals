﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialBlockAttributes : MonoBehaviour {

    public AudioClip Bubble, Hay;

    public Sprite bubbleSprite, haySprite;

    SpriteRenderer sr;

    private void Start()
    {
        //Define the spriterenderer
        sr = gameObject.GetComponent<SpriteRenderer>();

        //If the level is a sealevel
        if(Application.loadedLevel > 15 && Application.loadedLevel <= 30)
        {
            //Change the sprite and audio accordingly
            sr.sprite = bubbleSprite;
            GetComponent<AudioSource>().clip = Bubble;
        }//If the level is a farm level
        else if(Application.loadedLevel > 30 && Application.loadedLevel <= 45)
        {
            //Change the sprite and audio accordingly
            GetComponent<AudioSource>().clip = Hay;
            sr.sprite = haySprite;
        }
    }

    public void OnMouseOver()
    {
        //If the gameobject is clicked and the game is not paused
            if (Input.GetMouseButtonDown(0) && Time.timeScale == 1)
            {
            //Start the destruction process
                StartCoroutine(PlayAudio());
            }
    }

    private void OnMouseEnter()
    {
        //When mouse is over the gameobject, turn it transparent.
        //ONLY REALLY NOTICABLE/USEFUL IN UNIITY EDITOR
        Color tmp = sr.color;
        tmp.a = 0.5f;
        sr.color = tmp;
    }
    private void OnMouseExit()
    {
        //When mouse is leaves the gameobject, turn it back to its original colour.
        //ONLY REALLY NOTICABLE/USEFUL IN UNIITY EDITOR
        Color tmp = sr.color;
        tmp.a = 1f;
        sr.color = tmp;
    }

    IEnumerator PlayAudio()
    {
        //Play an audio clip, wait for a bit and destroy the object
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(0.1f);
        Destroy(gameObject);
    }
}
