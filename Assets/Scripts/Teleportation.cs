﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleportation : MonoBehaviour {

    public Transform telePos;
    GameObject plr;

	// Use this for initialization
	void Start () {
        plr = GameObject.FindWithTag("Player");
	}


    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            plr.transform.position = telePos.position;
        }
    }
}
