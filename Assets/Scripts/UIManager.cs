﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject MainMenu, LevelSelection, LineColorMenu, BG1, BG2, AnimalsAnim;


    public GameObject Gold, Silver, Bronze;

    public static bool MainMenuActive, LevelSelectionActive, LineColorMenuActive;

    public Text TimerText, WinTimeText;

    bool animStartable;

    void Start()
    {
        //Set timescale to 1.
        Time.timeScale = 1;
        //If level selection is not active, main menu is active.
        if(!LevelSelectionActive)
            MainMenuActive = true;

        if(Application.loadedLevel >= 1 && Application.loadedLevelName != "Credits")
            GameObject.Find("LevelNumber").GetComponentInChildren<Text>().text = Application.loadedLevel.ToString();
 
    }

    void Update()
    {
        /// Calls these methods.
        CheckMenu();
        CheckScore();
        DebugReset();
        ///
        
        //If current scene is 0, set Line color change menu inactive.
        if(Application.loadedLevel == 0)
        {

            if (LineColorMenuActive == false)
                LineColorMenu.SetActive(false);
            else
                LineColorMenu.SetActive(true);
        }

        //If current scene is 0 and line color change menu is active, then set the line color change menu background active.
        if(Application.loadedLevel == 0 && LineColorMenuActive == true)
        {
            BG1.SetActive(false);
            BG2.SetActive(true);
        }
        //Else if current scene is 0 set the main menu background active.
        else if(Application.loadedLevel == 0)
        {
            BG1.SetActive(true);
            BG2.SetActive(false);
        }

        //If main menu is active and current scene is 0, set animals in main menu active.
        if(MainMenuActive == true && Application.loadedLevel == 0)
        {
                AnimalsAnim.SetActive(true);
        }

        //If current scene is 0 and main menu is not active, set animation to startable and set animals in main menu inactive.
        if(Application.loadedLevel == 0 && MainMenuActive == false)
        {
            AnimalsAnim.SetActive(false);
        }
    }

    //Method for checking what menu is currently active.
    void CheckMenu()
    {
        //If main menu is active, set its position to 0,0,0 and move other menus away from camera.
        if(MainMenuActive == true && Application.loadedLevel == 0)
        {
            MainMenu.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
            LevelSelection.GetComponent<RectTransform>().localPosition = new Vector3(-2000, -2000, 0);
            LineColorMenu.GetComponent<RectTransform>().localPosition = new Vector3(2000, 2000, 0);
        }
        //If level selection is active, set its position to 0,0,0 and move other menus away from camera.
        if (LevelSelectionActive == true && Application.loadedLevel == 0)
        {
            MainMenu.GetComponent<RectTransform>().localPosition = new Vector3(2000, 2000, 0);
            LevelSelection.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
            LineColorMenu.GetComponent<RectTransform>().localPosition = new Vector3(2000, 2000, 0);
        }
        //If line color change menu is active, set its position to 0,0,0 and move other menus away from camera.
        if (LineColorMenuActive == true && Application.loadedLevel == 0)
        {
            MainMenu.GetComponent<RectTransform>().localPosition = new Vector3(2000, 2000, 0);
            LevelSelection.GetComponent<RectTransform>().localPosition = new Vector3(-2000, -2000, 0);
            LineColorMenu.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
        }
    }

    //Method for checking what star the player got.
    void CheckScore()
    {
        //Check if the current scene is a level and not Credits.
        if(Application.loadedLevel >= 1 && Application.loadedLevelName != "Credits")
        {
            //If gold from Goal script is true, set every star active.
            if (Goal.gold == true)
            {
                Gold.SetActive(true);
                Silver.SetActive(true);
                Bronze.SetActive(true);
            }
            //If silver from Goal script is true, set silver and bronze active and disable gold.
            else if (Goal.silver == true)
            {
                Gold.SetActive(false);
                Silver.SetActive(true);
                Bronze.SetActive(true);
            }
            //If bronze from Goal script is true, set only bronze active and disable silver and gold.
            else if (Goal.bronze == true)
            {
                Gold.SetActive(false);
                Silver.SetActive(false);
                Bronze.SetActive(true);
            }
        }

    }

    //Method for resetting all progress for debugging purposes.
    void DebugReset()
    {
        if (Input.GetKey(KeyCode.Alpha1))
        {
            PlayerPrefs.DeleteAll();
            SceneManager.LoadScene(Application.loadedLevel);
        }
    }
}
