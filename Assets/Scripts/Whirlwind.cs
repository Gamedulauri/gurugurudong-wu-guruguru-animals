﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Whirlwind : MonoBehaviour
{
    public float thrust;
    GameObject plr;

    // Use this for initialization
    void Start()
    {
        plr = GameObject.FindWithTag("Player");
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            plr.GetComponent<Rigidbody2D>().AddForce(transform.up * thrust);
        }
    }
}
